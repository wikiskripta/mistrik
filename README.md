# Mistríkovo skóre čtivosti

Jednoduchá webová aplikace na zjištění skóre čtivosti textu podle Mistríka.

- [webová aplikace]()
- [vysvětlení skóre](https://www.wikiskripta.eu/w/WikiSkripta:Sk%C3%B3re_%C4%8Dtivosti) ve WikiSkriptech

## Licence
Petr Kajzar, 1. lékařská fakulta UK, 2017-2018

Javascriptová knihovna _mistrik.js_ je dostupná pod licencí MIT.