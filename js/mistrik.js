/**
 * Výpočet Mistríkova skóre
 * JavaScriptová knihovna
 *
 * @file 
 * @author Petr Kajzar
 * @copyright First Faculty of Medicine, Charles University, Prague
 * @license MIT license
 *
 * MIT License [English]
 * 
 * Copyright 2017 Petr Kajzar, First Faculty of Medicine, Charles University, Prague
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


/**
 *
 * NÁVOD
 *
 * Mistríkovo skóre
 * Pro použití předejte text funkci mistrikScore.processArticle(article),
 * funkce vrací číselné Mistríkovo skóre zaokrouhlené na celá čísla.
 *
 */

var mistrikScore = {


    /**
     * Zpracování dat textu článku.
     * 
     * @param {article} Data předaná z HTML
     *
     * @return {int} Mistríkovo skóre
     */
    processArticle : function(article) {

        /* vyčistit text od nepotřebných znaků a řádků */
        article = mistrikScore.parseArticle(article);

        /* zjistit počet slov a počet různých slov */
        words = mistrikScore.getWords(article);
        countWords = words[0];
        countDiffWords = words[1];

        /* zjistit počet slabik */
        countSyllables = mistrikScore.getSyllables(article);

        /* zjistit počet vět */
        countSentences = mistrikScore.getSentences(article);

        /* spočítat Mistríkovo skóre */
        score = mistrikScore.done(countSyllables, countDiffWords, countWords, countSentences);

        /* předat hodnotu Mistríkova skóre */
        return score;

    },


    /**
     * Vyčistit text článku od znaků a nepotřebných řádků
     *
     * @param {article} Text článku
     *
     * @return {string} Čistý text článku bez nepotřebných znaků
     */
    parseArticle : function(article) {

        /* pole regulárních výrazů, které potřebujeme */
        regDeletions = {
            chars : /[\!\@\#\$\^\&\%\*\(\)\+\=\-\–\\\[\]\/\{\}\|\<\>\?\,\d\…]/g,    // speciální znaky
            white : /[\s]+/g,    // prázdné znaky
            doubles : /(ou|au)/ig,    // jednoslabičné dvojhlásky (eo, ie a ea apod. tvoří dvě slabiky: např. nausea)
            stop : /[\.\:\;]+/g,     // tečka, dvojtečka nebo středník na konci věty
        };

        /* převést tečky, dvojtečku a středník na jednu tečku (pro pozdější počet vět) */
        article = article.replace(regDeletions["stop"], ".");

        /* smazat nepotřebné znaky */
        article = article.replace(regDeletions["chars"], "");

        /* prázdné znaky zaměnit za jednu mezeru */
        article = article.replace(regDeletions["white"], " ");

        /* dvojhlásky zaměnit za jedno "o" třeba :-) */
        article = article.replace(regDeletions["doubles"], "o");

        /* smažeme prázdné znaky */
        article = article.trim();

        /* odešleme zpět */
        return article;

    },


    /**
     * Spočítat počet slov v textu
     *
     * @param {article} Čistý text článku
     *
     * @return {array} [počet slov v článku], [počet různých slov v článku]
     */
    getWords : function(article) {

        /* převést na malá písmena, ať můžu porovnávat */
        article = article.toLowerCase();

        /* rozdělit článek na slova */
        articleArray = article.split(" ");

        /* nejprve zjistíme počet slov v článku */
        countWords = articleArray.length;

        /* počet různých slov */
        diffWords = new Array();

        /* požiju prvních 5 znaků slova, pokud souhlasí, jsou to asi stejná slova */
        for (i = 0; i < articleArray.length; i++) {

            if (diffWords.indexOf(articleArray[i].substring(0,5)) === -1) {

                diffWords.push(articleArray[i].substring(0,5));

            }

        }
            
        countDiffWords = diffWords.length;

        /* a to je vše, vrátím zpět výsledky */
        return [countWords, countDiffWords];

    },


    /**
     * Spočítat počet vět v textu
     *
     * @param {article} Čistý text článku
     *
     * @return {int} Počet vět v článku
     */
    getSentences : function(article) {

        /* počet vět by měl skoro odpovídat počtu teček, které nejsou následovány mezerou a malým písmenem */
        countSentencesArray = article.match(/[\.](?!([\s]*[a-zžščřďňťáéóúí]))/g);

        /* pokud nenajdeme tečku, tak to považujme za jednu větu */
        if (countSentencesArray === null) return 1;

        /* předat počet vět zpět */
        return countSentencesArray.length;

    },


    /**
     * Spočítat počet slabik v textu
     *
     * @param {article} Čistý text článku
     *
     * @return {int} Počet slabik v textu
     */
    getSyllables : function(article) {

        /* samohlásky a slabikotvorné souhlásky */
        splitVowels = new Array("a", "á", "e", "é", "ě", "o", "ó", "u", "ú", "ů", "i", "í", "y", "ý");
        splitConsonants = /[bcčdďfghjklmnňpqrřsštťvwxzž][lr][bcčdďfghjklmnňpqrřsštťvwxzž]/ig;
        splitNasals = /[bcčdďfghjklpqrřsštťvwxzž][mnň][^aáeéěoóuúůiíyý]/ig;

        /* počet slabik je na začátku 0 */
        countSyllables = 0;

        /* počet slabik v článku */
        for (i = 0; i < splitVowels.length; i++) {

            /* seznam výskytů samohlásky */
            listSyll = article.match(new RegExp(splitVowels[i], "g"));

            /* počet slabik víceméně odpovídá počtu samohlásek (dvojhlásky jsou už zrušeny v processArticle()) */
            if (listSyll !== null) {
                countSyllables += listSyll.length;
            }

        }

        /* zkusím spočítat slabikotvorné souhlásky */
        splitConsonantArray = article.match(splitConsonants);
        if (splitConsonantArray !== null) countSyllables += splitConsonantArray.length;

        /* zkusím spočítat slabikotvorné nosovky */
        splitNasalsArray = article.match(splitNasals);
        if (splitNasalsArray !== null) countSyllables += splitNasalsArray.length;

        /* a to je vše, poslat výsledek zpět */
        return countSyllables;
            
    },


    /**
     * Spočítat Mistríkovo skóre
     *
     * @param {countSyllables} Počet slabik
     * @param {countDiffWords} Počet různých slov
     * @param {countWords} Počet slov
     * @param {countSentences} Počet vět
     *
     * @return {int} Mistríkovo skóre
     */
    done : function(countSyllables, countDiffWords, countWords, countSentences) {

        return Math.round(50 - (countSyllables * countDiffWords / countWords / countSentences));

    }

}
