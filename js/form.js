/**
 * Získávání textu pro výpočet Mistríkova skóre
 * (samotný výpočet používá javascriptovou knihovnu "mistrik.js")
 *
 * @file 
 * @author Petr Kajzar
 * @copyright First Faculty of Medicine, Charles University, Prague
 * @license MIT license
 *
 * MIT License [English]
 * 
 * Copyright 2017 Petr Kajzar, First Faculty of Medicine, Charles University, Prague
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */


/* zpracování formuláře */

var processForm = {

    /**
     * získat text článku
     */
    getText : function() {

        /* text se nachází v poli formuláře, na konec dám tečku, pokud tam není */
        article = document.forms[0].elements[0].value;
        if (/[\.][\s]?$/.test(article) === false) article += ".";


        /* prázdný text */
        if (article.length < 50) {
           window.alert("Text je příliš krátký.");
           return false;
        }

        /* zpracovat */
        score = mistrikScore.processArticle(article);

        /* vložit na stránky */
        processForm.inputScore(score);

    },  


    /**
     * Vložit Mistríkovo skóre na stránky
     *
     * @param {score} Skóre
     */
    inputScore : function(score) {

        /* cílový span */
        span = document.getElementById("skore");

        /* barva skóre */
        if (score >= 40) span.style.borderColor = "#2ecc40";
            else if (score >= 30) span.style.borderColor = "#01ff70";
            else if (score >= 20) span.style.borderColor = "#ffdc00";
            else if (score >= 10) span.style.borderColor = "#ff851b";
            else if (score < 10) span.style.borderColor = "#ff4136";

        /* vložení do stránky */
        span.innerText = score;
        document.getElementById("vysledek").style.opacity = 1;

    },


    /**
     * Upravit chování formuláře
     */
    submitActions : function() {

        /* neodesílat formulář na server */
        document.forms[0].onsubmit = function() {return false;};

        /* spustit skript při kliknutí na button */
        document.forms[0].getElementsByTagName("button")[0].onclick = function() {processForm.getText();};

    },

}


/* spustit při startu */
if (document.readyState !== "loading") {
    processForm.submitActions();
} else {
    document.addEventListener('DOMContentLoaded', processForm.submitActions);
}